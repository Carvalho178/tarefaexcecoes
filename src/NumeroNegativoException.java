
public class NumeroNegativoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NumeroNegativoException(){
		super("Numero negativo");
	}

}