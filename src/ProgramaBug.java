import java.util.InputMismatchException;
import java.util.Scanner;

public class ProgramaBug {
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		try {
			System.out.println("Digite sua idade");
			int Idade = Integer.parseInt(entrada.next());
			new ChecarDados(Idade);
		}catch (InputMismatchException e) {
			System.out.println("Digite um numero.");
		}catch(NumeroNegativoException e){
			System.out.println("Digite um numero valido");
			System.err.println(e.getMessage());
		}catch (Exception e) {
			System.out.println("Erro Desconhedido");
		}
		
		try {
			System.out.println("Digite uma Data");
			String Data = entrada.next();
			new ChecarDados(Data);
		}catch(DataException e) {
			System.out.println("Digite uma Data valida");
			System.err.println(e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro Desconhecido");
		}
		
		
		System.out.println("Nao Quebrou");
		entrada.close();
	}
	
}
