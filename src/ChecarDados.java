

public class ChecarDados {
	
	public ChecarDados(int numero) throws NumeroNegativoException {
		if (numero < 0) {
			throw new NumeroNegativoException();
		}	
	}
	
	public ChecarDados(String Data) throws DataException{
		int valor;
		if(Data.length() != 10) {
			throw new DataException();
		}
		String CamposData[] = new String[3];
		CamposData = Data.split("/");
		
		if(CamposData[0].length() != 2 ) {
			throw new DataException(0);
		}
		else {
			try {
				valor = Integer.parseInt(CamposData[0]);
			}catch(Exception e) {
				throw new DataException(1, CamposData[0]);
			}
			if(valor < 1 || valor > 31) {
				throw new DataException(0, valor);
			}
		}
		if(CamposData[1].length() != 2) {
			throw new DataException(1);
		}
		else {
			try {
				valor = Integer.parseInt(CamposData[1]);
			}catch(Exception e) {
				throw new DataException(1, CamposData[1]);
			}
			if(valor < 1 || valor > 12) {
				throw new DataException(1, valor);
			}
		}
		if(CamposData[2].length() != 4) {
			throw new DataException(2);
		}
		else {
			try {
				valor = Integer.parseInt(CamposData[2]);
			}catch(Exception e) {
				throw new DataException(2, CamposData[2]);
			}
			if(valor < 1) {
				throw new DataException(2, valor);
			}
			
		}
			
		                                                                                            
			
	}
	
}