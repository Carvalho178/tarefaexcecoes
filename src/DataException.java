
public class DataException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DataException(){
		super("Formato Data Errado - qtd de digitos");
		System.err.println("Formato Errado - qtd de digitos");
		System.err.println("Formato Padrao DD/MM/AAAA");
	}
	public DataException(int campo){
		super("Formato em campos data errado - qtd de digitos");
		String iString;
			if(campo == 0) {
				iString = "dia";
			}
			else if (campo == 1) {
				iString = "mes";
			}
			else if(campo == 2) {
				iString = "ano";
			}
			else {
				iString = "campo";
			}
		System.err.println(iString + " invalido");
		System.err.println("Formato Padrao DD/MM/AAAA");
	}
	public DataException(int campo, String palavra) {
		super("Campo contendo Letra");
		String iString;
		switch (campo) {
			case 1:
				iString = "dia";
				break;
			case 2: 
				iString = "mes";
				break;
			case 3:
				iString = "ano";
				break;
			default:
				iString = "campo";
		}
		System.err.println(iString + " invalido, contendo letras");
	}
	public DataException(int campo, int valor) {
		super("Valor incorreto para campo");
		String iString;
		switch (campo) {
			case 1:
				iString = " ,Dia só entre 1 - 31";
				break;
			case 2: 
				iString = " ,Mes apenas entre 1 - 12";
				break;
			case 3:
				iString = " ,Ano nao pode ser negativo.";
				break;
			default:
				iString = " ,Campo nao existe";
		}
		System.err.println("Valor "+ valor + iString);
		
	}
	
}
